import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      long start = System.currentTimeMillis();

      Graph g = new Graph ("G");

      g.createRandomSimpleGraph (2100, 2099, true);

      System.out.println(g.checkVertexForRoot("v190"));
      long finish = System.currentTimeMillis();
      long timeElapsed = finish - start;
      System.out.println("Elapsed time: " + timeElapsed + "ms");


   }

   /**
    * Check that given Graph is a tree with  given vertex as root.
    */

   /** Vertex represents one vertex in the graph.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create a connected directed random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomDirectedTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                       + varray [i].toString(), varray [vnr], varray [i]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       * @param directed directed or undirected graph will be
       */
      public void createRandomSimpleGraph (int n, int m, boolean directed) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         if (directed){
            createRandomDirectedTree(n);
         }else {
            createRandomTree (n);       // n-1 edges created here
         }
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;// remaining edges

         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Check if Graph is tree.
       * @return boolean
       */
      public boolean graphIsTree(){
         return getTotalVertex() - 1 == getTotalArc();

      }

      /**
       * Check if vertex is root of a tree.
       * @param vertexId vertex id to check
       * @return boolean
       */
      public boolean checkVertexForRoot(String vertexId){
         if (!graphIsTree()){
            throw new RuntimeException("Graph is not tree!");
         }
         Vertex v = getVertexById(vertexId);
         Vertex current = first;
         int count= 0;
         while(current != null){
            Arc a = current.first;
            while (a != null){
               if (a.target == v){
                  count++;
               }
               a = a.next;
            }
            current= current.next;
         }
         return count == 0;
      }

      /**
       * Get number of vertexes in graph.
       * @return int of total vertexes
       */
      public int getTotalVertex(){

         int count = 0;
         Vertex v = first;
         while (v != null){
            count++;
            v= v.next;
         }
         return count;
      }

      /**
       * Get number of arcs in graph.
       * @return int of total arcs.
       */
      public int getTotalArc(){

         int count = 0;
         Vertex v = first;
         while (v != null) {
            Arc a = v.first;
            while (a != null) {
               count++;
               a = a.next;
            }
            v=v.next;
         }
         return count;
      }

      /**
       * Get vertex object by its id.
       * @param id name of vertex
       * @return Vertex
       */
      public Vertex getVertexById(String id){
         Vertex v = first;
         while (v != null){
            if (v.id.equals(id))
               return v;
            v=  v.next;
         }
         throw new RuntimeException(String.format("Graph: %s does not have Vertex: %s", this.id, id));
      }
   }

} 

